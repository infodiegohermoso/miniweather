package com.infodiegohermoso.miniweather.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.infodiegohermoso.miniweather.API.API;
import com.infodiegohermoso.miniweather.API.service.WeatherService;
import com.infodiegohermoso.miniweather.R;
import com.infodiegohermoso.miniweather.models.City;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private AppCompatEditText etSearch;
    private TextView tvNameCity;
    private TextView tvStatusCity;
    private ImageView ivStatusCity;
    private TextView tvCelsius;
    private Button btnSearch;

    private Call<City> cityCall;
    private WeatherService service;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicializar();

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String city = etSearch.getText().toString();

                if (city != ""){
                    cityCall = service.getCity(city,API.APP_KEY,"metric","es");
                    cityCall.enqueue(new Callback<City>() {
                        @Override
                        public void onResponse(Call<City> call, Response<City> response) {
                            City city =response.body();

                            setResult(city);
                        }

                        @Override
                        public void onFailure(Call<City> call, Throwable t) {
                            Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_LONG).show();

                        }
                    });
                }
            }
        });
    }




    private void inicializar(){

        etSearch = findViewById(R.id.etSearchCity);
        tvNameCity = findViewById(R.id.tvNameCity);
        tvCelsius = findViewById(R.id.tvWeatherCelsius);
        ivStatusCity = findViewById(R.id.ivWeatherStatus);
        tvStatusCity = findViewById(R.id.tvWeatherStatus);
        btnSearch = findViewById(R.id.btnSearch);

        service = API.getApi().create(WeatherService.class);
    }

    private void setResult(City city){

        tvNameCity.setText(city.getName());
        tvStatusCity.setText(city.getDesc());
        tvCelsius.setText(String.valueOf(city.getTemp()) + " ºC");
        Picasso.get().load(API.BASE_ICON + city.getIcon() + API.EXTENSION_ICONS).into(ivStatusCity);
    }
}
