package com.infodiegohermoso.miniweather.API;

import com.google.gson.GsonBuilder;
import com.google.gson.internal.GsonBuildConfig;
import com.infodiegohermoso.miniweather.API.Deserializers.CityDeserializer;
import com.infodiegohermoso.miniweather.models.City;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class API {

    public static final String BASE_URL= "https://api.openweathermap.org/data/2.5/";
    public static final String BASE_ICON = "https://openweathermap.org/img/w/";
    public static final String EXTENSION_ICONS = ".png";
    public static final String APP_KEY = "555343654fffc73d994b773c92b9d7c8";

    public static Retrofit retrofit = null;



    public static Retrofit getApi(){

        if (retrofit == null){

            GsonBuilder builder = new GsonBuilder();

            builder.registerTypeAdapter(City.class,new CityDeserializer());

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(builder.create()))
                    .build();

        }

        return retrofit;
    }

}
