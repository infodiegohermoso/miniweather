package com.infodiegohermoso.miniweather.API.service;

import com.infodiegohermoso.miniweather.models.City;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherService {

        @GET("weather")
        Call<City> getCity(@Query("q") String city, @Query("appid") String key, @Query("units") String value, @Query("lang") String lang);
    }
