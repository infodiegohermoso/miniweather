package com.infodiegohermoso.miniweather.models;

public class City {
    private int id;
    private String name;
    private String desc;
    private float temp;
    private String icon;

    public City(int id, String name, String desc, float temp, String icon) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.temp = temp;
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public float getTemp() {
        return temp;
    }

    public void setTemp(float temp) {
        this.temp = temp;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
